﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using ChildProcessManager;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting");
            if (ChildProcess.IsChildProcess())
            {
                RunAsChildProcess();
            }
            else
            {
                RunAsProcessManager();
            }
        }

        private static void RunAsProcessManager()
        {
            var lastMessage = DateTime.Now;
            ThreadPool.SetMinThreads(300, 300);
            Thread.Sleep(10);
            var writeToConsole = new ConcurrentQueue<string>();

            Console.WriteLine("Starting Manager");
            ProcessManager.ChildDebuggerAutoAttach = false;
            var children = new List<ChildProcessInfo>();

            var processorsForNewChildren = new RequestProcessors();
            //add processors here
            processorsForNewChildren.RegisterRequestWithNoResponseProcessor<string>(50, (source, request) =>
            {
                writeToConsole.Enqueue($"{source?.IpcId} - Sever shared command 50 - {request}");
                lastMessage = DateTime.Now;
            });
            processorsForNewChildren.RegisterRequestProcessor<string, string>(30, (source, request) =>
            {
                writeToConsole.Enqueue($"Got command 30 from child {source?.IpcId} - {request}");
                lastMessage = DateTime.Now;
                return new Response<string>() { Value = "CMD 40 SRV Response" };
            });

            var manager = new ProcessManager(processorsForNewChildren);
            
            manager.ProcessStateChanged += (sender, args) =>
            {
                writeToConsole.Enqueue($"{args.ChildProcessInfo.IpcId} - {args.Action} - {args.Data}");
                lastMessage = DateTime.Now;
            };

            for (int i = 0; i < 100; i++)
            {
                string name= $"Child_{i}";
                var startInfo = new ProcessStartInfo("dotnet", System.Reflection.Assembly.GetEntryAssembly().Location);
                startInfo.WorkingDirectory = Path.GetDirectoryName(startInfo.FileName);
                var child = manager.StartChildProcess(startInfo, info =>
                {
                    info.Name = name;
                    info.Processors.RegisterRequestWithNoResponseProcessor<string>(40, (source, request) =>
                    {
                        writeToConsole.Enqueue($"Got command 40 from child {source?.IpcId} ({info.Name}) - {request}");
                        lastMessage = DateTime.Now;
                    });
                    lastMessage = DateTime.Now;
                });
                children.Add(child);
                //Thread.Sleep(TimeSpan.FromMilliseconds(1000));
            }

            SpinWait.SpinUntil(()=>
            {
                bool condition = lastMessage < DateTime.Now.AddSeconds(-5);
                while (writeToConsole.TryDequeue(out var message))
                {
                    Console.WriteLine(message);
                    condition = false;
                    lastMessage = DateTime.Now;
                }
                return condition;
            });
            Console.WriteLine("Press Enter to to get count of running children");
            Console.ReadLine();
            Console.WriteLine($"{children.Where(x=>x.ProcessIsAlive).Count()} children still running.");

            Console.WriteLine("Press Enter to request 1 child to shutdown");
            Console.ReadLine();
            var livingChild = children.FirstOrDefault(x => x.ProcessIsAlive);
            if (livingChild != null)
            {
                livingChild.Shutdown();
                if (livingChild.ProcessIsAlive)
                {
                    Console.WriteLine("Child ignored shutdown command.");
                }
                else
                {
                    Console.WriteLine("Child shutdown successfully.");
                }
            }
            else
            {
                Console.WriteLine("No children alive to shutdown.");
            }

            Console.WriteLine($"{children.Where(x => x.ProcessIsAlive).Count()} children still running.");
            Console.WriteLine("Press enter to start shutting down");
            Console.ReadLine();
            Console.WriteLine("Exiting Manager so all other children should exit");
            manager.Dispose();
            SpinWait.SpinUntil(() => lastMessage < DateTime.Now.AddSeconds(-2));
            Console.WriteLine("All child processes should be stopped now. Press enter to exit");
            Console.ReadLine();
        }

        private static void RunAsChildProcess()
        {
            ThreadPool.SetMinThreads(10, 10);
            Console.WriteLine("Starting Child");
            ManualResetEventSlim exitEvent = new ManualResetEventSlim();
            ChildProcess cp = null;
            try
            {


             
             ChildProcessStateChangedEnum? lastState = null;
            var processors = new RequestProcessors();
                //add processors here


             cp = new ChildProcess((state, info) =>
            {
                Console.WriteLine($"Child State - {state} - {info}");
                lastState = state;
                switch (state)
                {
                    case ChildProcessStateChangedEnum.ParentExited:
                        exitEvent.Set();
                        break;
                    case ChildProcessStateChangedEnum.WatchdogTimeout:
                        exitEvent.Set();
                        break;
                    case ChildProcessStateChangedEnum.PingFailed:
                        exitEvent.Set();
                        break;
                    case ChildProcessStateChangedEnum.ShutdownRequested:
                        exitEvent.Set();
                        break;
                }
            }, processors);

            cp.SendRequestWithoutResponse(50, $"Test from {cp.IpcId}");
            cp.SendRequestWithoutResponse(40, $"Test from {cp.IpcId}");
            var response = cp.SendRequest<string, string>(30, $"Test from {cp.IpcId}");
            if (response == null || response.IsError)
            {
                Console.WriteLine($"Command 30 (took {response?.IpcTimeTaken.TotalMilliseconds}ms) - errored - {response?.Error}");
            }
            else
            {
                Console.WriteLine($"Command 30 (took {response?.IpcTimeTaken.TotalMilliseconds}ms) - response - {response.Value}");
            }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Fatal Unhandled Exception - {e.Message}");
            }

            exitEvent.Wait();
            //SpinWait.SpinUntil(()=>exit);
            //changed above to below to make it use cpu so i can find them
            //while (!exit)
            //{
            //    //Thread.Sleep(5);
            //}

            Console.WriteLine("Stopping Child");
            cp?.Dispose();
            Console.WriteLine("Stopped Child");
        }
    }
}
