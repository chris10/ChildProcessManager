﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using GroBuf;
using GroBuf.DataMembersExtracters;
using tiesky.com;

namespace ChildProcessManager
{
   
    public class IpcInstance : IDisposable
    {

        private static readonly Serializer Serializer = new Serializer(new PropertiesExtractor());
        public string IpcId { get; }
        public IRequestProcessors Processors { get; }
        private SharmIpc sm = null;
        public DateTime? LastMessageReceivedAt { get; private set; }

        public IpcInstance(string ipcId, IRequestProcessors processors)
        {
            IpcId = ipcId;
            Processors = processors ?? throw new ArgumentNullException(nameof(processors));
            sm = new SharmIpc("Global/" + IpcId, ReceivedRemoteCall, ExternalExceptionHandler: (str, ex) =>
            {
                Console.WriteLine($"String:{str}, Exception:{ex}");
            });
        }

        public Response<TResponse> SendRequest<TRequest, TResponse>(byte command, TRequest request, int timeoutMs = 30000)
        {
            var requestBytes = Serializer.Serialize(command, request);
            var sw = new Stopwatch();
            sw.Start();
            var result = sm.RemoteRequest(requestBytes, null, timeoutMs);
            sw.Stop();
            if ((result.Item2?.Length ?? 0) > 0)
            {
                var response = Serializer.Deserialize<Response<TResponse>>(result.Item2);
                response.IpcTimeTaken = sw.Elapsed;
                return response;
            }
            else if(result.Item1)
            {
                //got success it was just an empty response
                return new Response<TResponse>(){IpcTimeTaken = sw.Elapsed};
            }
            else
            {
                return new Response<TResponse>(){Error = new Error(){ErrorType = ErrorType.ProcessManagerError, ErrorCode = 3, Message = $"Send Failed - Usage Report ({sw.Elapsed.TotalMilliseconds}ms)- {sm.UsageReport()}" }, IpcTimeTaken = sw.Elapsed };
            }
        }

        /// <summary>
        /// Send a request and expects no response
        /// </summary>
        /// <typeparam name="TRequest"></typeparam>
        /// <param name="command"></param>
        /// <param name="request"></param>
        /// <returns>true if request was sent</returns>
        public bool SendRequestWithoutResponse<TRequest>(byte command, TRequest request)
        {
            var requestBytes = Serializer.Serialize(command, request);
            var result = sm.RemoteRequestWithoutResponse(requestBytes);
            return result;
        }

        /// <summary>
        /// non-blocking requests
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private Tuple<bool, byte[]> ReceivedRemoteCall(byte[] data)
        {
            try
            {

            
            if ((data?.Length ?? 0) < 1)
            {
                //invalid request
                var res = new Response<object>() { Error = Error.GenerateError(ErrorType.ProcessManagerError, "No data provided in request so request invalid.", 1) };
                return new Tuple<bool, byte[]>(true, Serializer.Serialize(res));
            }

            LastMessageReceivedAt = DateTime.Now;

            int index = 0;
            var command = Serializer.Deserialize<byte>(data, ref index);

            Processor processor = Processors.GetProcessor(command);
            if (processor == null)
            {
                //no processor found or invalid request
                var res = new Response<object>() { Error = Error.GenerateError(ErrorType.ProcessManagerError, $"No processor registered for command '{command}' in the request so request rejected.", 2) };
                Console.WriteLine("Error - " + res.Error.Message);
                return new Tuple<bool, byte[]>(true, Serializer.Serialize(res));
            }
            var request = Serializer.Deserialize(processor.RequestType, data, ref index);
            var response = processor.RequestProcessor(request);
            if (response == null)
            {
                return new Tuple<bool, byte[]>(true, null);
            }
            else
            {
                return new Tuple<bool, byte[]>(true, Serializer.Serialize(response));
            }
            }
            catch (Exception e)
            {
                var res = new Response<object>() { Error = Error.GenerateError(ErrorType.ProcessManagerError, $"Unhandled exception processing request. {e}", 4) };
                Console.WriteLine("Error - " + res.Error.Message);
                return new Tuple<bool, byte[]>(true, Serializer.Serialize(res));
            }
        }

        public void Dispose()
        {
            sm?.Dispose();
        }
    }
}
