﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChildProcessManager
{
    public interface ISource
    {
        string IpcId { get; }
        int ProcessId { get; }
        RequestProcessors Processors { get; }
        Response<TResponse> SendRequest<TRequest, TResponse>(byte command, TRequest request, int timeoutMs = 30000);
        bool SendRequestWithoutResponse<TRequest>(byte command, TRequest request);
    }
}
