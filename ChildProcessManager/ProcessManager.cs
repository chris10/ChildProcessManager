﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace ChildProcessManager
{
    /// <summary>
    ///     The process manager.
    /// </summary>
    public class ProcessManager : IEnumerable, IDisposable
    {
        public RequestProcessors ProcessorsForAllNewChildren { get; }

        #region Fields

        /// <summary>
        ///     The child processes.
        /// </summary>
        private readonly Dictionary<int, ChildProcessInfo> _childProcesses;

        /// <summary>
        ///     The child processes lock.
        /// </summary>
        private readonly object _childProcessesLock;

        /// <summary>
        /// The watchdog timer.
        /// </summary>
        private Timer _watchdog;
        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessManager" /> class.
        /// </summary>
        /// <exception cref="InvalidOperationException"></exception>
        public ProcessManager(RequestProcessors processorsForAllNewChildren = null)
        {
            ProcessorsForAllNewChildren = processorsForAllNewChildren??new RequestProcessors();
            _childProcesses = new Dictionary<int, ChildProcessInfo>();
            _childProcessesLock = new object();
            _watchdog = new Timer(x=>OnWatchdog(), null, TimeSpan.FromSeconds(WatchDogTimeoutInSeconds), TimeSpan.FromSeconds(WatchDogTimeoutInSeconds/2));
        }

        #endregion

        #region Delegates

        /// <summary>
        ///     The process state changed event handler.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        public delegate void ProcessStateChangedEventHandler(object sender, ProcessStateChangedEventArgs e);

        #endregion

        #region Public Events

        /// <summary>
        ///     The child process started.
        /// </summary>
        public static event EventHandler<ProcessStartEventArgs> ChildProcessStarted;

        /// <summary>
        ///     The process state changed.
        /// </summary>
        public event EventHandler<ProcessStateChangedEventArgs> ProcessStateChanged;

        #endregion

        #region Public Properties

        public int WatchDogTimeoutInSeconds => 60;

        /// <summary>
        ///     Gets or sets a value if the child should be signaled that a debugger will automatically attach after start. Child
        ///     will wait until the debugger attaches.
        /// </summary>
        public static bool ChildDebuggerAutoAttach { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get enumerator.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerator" />.
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            lock (_childProcessesLock)
            {
                return _childProcesses.Values.ToArray().GetEnumerator();
            }
        }

        /// <summary>
        /// The start child process.
        /// </summary>
        /// <param name="processInfo">
        /// The process info.
        /// </param>
        /// <param name="addProcessorsBeforeStart">Called after ChildProcessInfo created but before the actual child process starts.</param>
        /// <returns>
        /// The <see cref="ChildProcessInfo"/>.
        /// </returns>
        public ChildProcessInfo StartChildProcess(ProcessStartInfo processInfo, Action<ChildProcessInfo> addProcessorsBeforeStart = null)
        {
            var childProcess = new ChildProcessInfo(ProcessorsForAllNewChildren);
            addProcessorsBeforeStart?.Invoke(childProcess);
            StartChildProcess(processInfo, childProcess);
            return childProcess;
        }

        /// <summary>
        /// The start child process.
        /// </summary>
        /// <param name="processInfo">
        /// The process info.
        /// </param>
        /// <param name="childProcessInfo">
        /// The child process.
        /// </param>
        public void StartChildProcess(ProcessStartInfo processInfo, ChildProcessInfo childProcessInfo)
        {
            processInfo.CreateNoWindow = false;
            processInfo.RedirectStandardOutput = true;
            processInfo.RedirectStandardInput = true;
            processInfo.RedirectStandardError = true;
            processInfo.UseShellExecute = false;
            processInfo.EnvironmentVariables.Add(CommandsAndConst.EnvIpcIdName, childProcessInfo.IpcId);
            var debug = ChildDebuggerAutoAttach && Debugger.IsAttached;
            processInfo.EnvironmentVariables.Add(CommandsAndConst.EnvAutoAttachDebuggerName, debug ? "True" : "False");
            
            var process = new Process {StartInfo = processInfo};
            childProcessInfo.PreStartInit(this, process);
            process.Start();
            try
            {
                childProcessInfo.PostStartInit();
                lock (_childProcessesLock)
                {
                    _childProcesses.Add(process.Id, childProcessInfo);
                }
                ChildProcessStarted?.Invoke(null, new ProcessStartEventArgs() { Manager = this, ProcessStartInfo = processInfo, StartedProcessInfo = childProcessInfo });
            }
            catch (Exception)
            {
                process.Kill();
                throw;
            }
        }

        /// <summary>
        /// The try get child process.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ChildProcessInfo"/>.
        /// </returns>
        public ChildProcessInfo TryGetChildProcess(int id)
        {
            lock (_childProcessesLock)
            {
                ChildProcessInfo childProcessInfo = null;
                _childProcesses.TryGetValue(id, out childProcessInfo);
                return childProcessInfo;
            }
        }

        #endregion

        #region Methods

        

        /// <summary>
        /// The raise process state changed event.
        /// </summary>
        /// <param name="childProcessInfo">
        /// The child process.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        protected internal virtual void RaiseProcessStateChangedEvent(ChildProcessInfo childProcessInfo, ManagerProcessStateChangedEnum action, string data)
        {
            if (action == ManagerProcessStateChangedEnum.ChildShutdown || action == ManagerProcessStateChangedEnum.ChildExited)
            {
                lock (_childProcessesLock)
                {
                    _childProcesses.Remove(childProcessInfo.ProcessId);
                }
            }

            // Raise the event by using the () operator.
            ProcessStateChanged?.Invoke(this, new ProcessStateChangedEventArgs(childProcessInfo, action, data));
        }


        /// <summary>
        /// The on watchdog.
        /// </summary>
        protected void OnWatchdog()
        {
            var exitedProcesses = new List<ChildProcessInfo>();
            ChildProcessInfo[] currentChildProcessesInfo;
            lock (_childProcessesLock)
            {
                currentChildProcessesInfo = _childProcesses.Values.ToArray();
            }

            foreach (var childProcess in currentChildProcessesInfo)
            {
                if (childProcess.Process!= null && !childProcess.ProcessIsAlive)
                {
                    exitedProcesses.Add(childProcess);
                }
                else
                {
                    if (childProcess.LastTimeAlive.HasValue && !childProcess.SendKeepAlive())
                    {
                        RaiseProcessStateChangedEvent(childProcess, ManagerProcessStateChangedEnum.ChildPingFailed, null);
                    }

                    if (childProcess.WatchdogTimeout)
                    {
                        RaiseProcessStateChangedEvent(childProcess, ManagerProcessStateChangedEnum.WatchdogTimeout, null);
                    }
                }
            }

            // Exited Child Processes 
            foreach (var exitedProcess in exitedProcesses)
            {
                RaiseProcessStateChangedEvent(exitedProcess, ManagerProcessStateChangedEnum.ChildExited, null);
            }

            lock (_childProcessesLock)
            {
                foreach (var exitedProcess in exitedProcesses)
                {
                    _childProcesses.Remove(exitedProcess.Process.Id);
                    exitedProcess.Dispose();
                }
            }
        }

        public void Dispose()
        {
            lock (_childProcessesLock)
            {
                if (_watchdog != null)
                {
                    _watchdog.Dispose();
                    _watchdog = null;
                }
                else
                {
                    //already disposing
                    return;
                }

                foreach (var childProcess in _childProcesses)
                {
                    childProcess.Value.SendParentShuttingDown();
                }
            }

            //give children 10 seconds to exit on their own
            SpinWait.SpinUntil(() => { lock (_childProcessesLock)
            {
                return _childProcesses.Count(x => x.Value.ProcessIsAlive) == 0;
            }}, TimeSpan.FromSeconds(10));
            
            //kill children that are still running
            lock (_childProcessesLock)
            {
                foreach (var childProcess in _childProcesses.ToArray())
                {
                    if (childProcess.Value.ProcessIsAlive)
                    {
                        childProcess.Value.Shutdown(true);
                    }
                }
            }
        }
        #endregion
    }
}