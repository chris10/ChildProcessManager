﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChildProcessManager
{
    public enum ErrorType { UnhandledException, ProcessManagerError, ApplicationError}
    public class Error
    {
        public ErrorType ErrorType { get; set; }
        public string Message { get; set; }
        public int? ErrorCode { get; set; }
        public Dictionary<string,string> ErrorParams { get; set; }

        public Error()
        {
            ErrorParams = new Dictionary<string, string>();
        }

        public override string ToString()
        {
            return $"ErrorType:{ErrorType}, ErrorCode:{ErrorCode}, Message:{Message}";
        }

        public static Error GenerateError(ErrorType errorType, Exception ex)
        {
            var error = new Error()
            {
                ErrorType = errorType,
                Message = "Exception:" + ex.Message
            };
            error.ErrorParams["Exception"] = ex.ToString();
            return error;
        }

        public static Error GenerateError(ErrorType errorType, string message, int? errorCode = null)
        {
            var error = new Error()
            {
                ErrorType = errorType,
                Message = message,
                ErrorCode = errorCode
            };
            return error;
        }
    }
}
