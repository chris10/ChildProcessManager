using ChildProcessManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTests
{
    public class IpcInstanceTests
    {
        [Fact]
        public void BasicRequestResponseTest()
        {
            string ipcId = Guid.NewGuid().ToString("N");
            var expectedMessages = new List<string>()
            {
                "Ping from Slave",
                "Pong from Master",
                "Ping from Master",
                "Pong from Slave",
                "Master get null from child"
            };
            List<string> messages = new List<string>();

            IpcInstance ipcMaster = null;
            IpcInstance ipcSlave = null;
            var masterProcessors = new RequestProcessors();
            var slaveProcessors = new RequestProcessors();


            masterProcessors.RegisterRequestProcessor<string, string>(0, (source, request) =>
            {
                messages.Add(request);
                return new Response<string>() { Value = "Pong from Master" };
            });


            slaveProcessors.RegisterRequestProcessor<string, string>(0, (source, request) =>
            {
                messages.Add(request);
                return new Response<string>() { Value = "Pong from Slave" };
            });

            slaveProcessors.RegisterRequestProcessor<string, string>(1, (source, request) =>
            {
                messages.Add(request);
                return new Response<string>() { Value = null };
            });



            ipcMaster = new IpcInstance(ipcId, masterProcessors);
            Task.Run(() => { ipcSlave = new IpcInstance(ipcId, slaveProcessors);}).Wait();

            var response = ipcSlave.SendRequest<string, string>(0, "Ping from Slave");
            Assert.False(response.IsError);
            messages.Add(response.Value);

            response = ipcMaster.SendRequest<string, string>(0, "Ping from Master");
            Assert.False(response.IsError);
            messages.Add(response.Value);

            response = ipcMaster.SendRequest<string, string>(1, "Master get null from child");
            Assert.False(response.IsError);
            Assert.Null(response.Value);

            Assert.Equal(expectedMessages, messages);

            ipcMaster?.Dispose();
            ipcSlave?.Dispose();
        }

        [Fact]
        public void SpeedTest()
        {
            int workerThreads, completionPortThreads;
            ThreadPool.GetMinThreads(out workerThreads, out completionPortThreads);
            ThreadPool.SetMinThreads(50, 10);
            var func = new Func<TimeSpan>(()=>
            {
                
                string ipcId = Guid.NewGuid().ToString("N");
                IpcInstance ipcMaster = null;
                IpcInstance ipcSlave = null;
                var masterProcessors = new RequestProcessors();
                var slaveProcessors = new RequestProcessors();


                masterProcessors.RegisterRequestProcessor<string, string>(0, (source, request) =>
                {
                    return new Response<string>() { Value = "Pong from Master" };
                });


                slaveProcessors.RegisterRequestProcessor<string, string>(0, (source, request) =>
                {
                    return new Response<string>() { Value = "Pong from Slave" };
                });

                ipcMaster = new IpcInstance(ipcId, masterProcessors);
                Task.Run(() => { ipcSlave = new IpcInstance(ipcId, slaveProcessors); }).Wait();

                var sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < 100; i++)
                {
                    var response = ipcSlave.SendRequest<string, string>(0, "Ping from Slave");
                    Assert.False(response.IsError);
                }

                for (int i = 0; i < 100; i++)
                {
                    var response = ipcMaster.SendRequest<string, string>(0, "Ping from Master");
                    Assert.False(response.IsError);
                }
                sw.Stop();
                ipcMaster?.Dispose();
                ipcSlave?.Dispose();
                return sw.Elapsed;
            });

            var baseline = func();
            long maxMs = 2000;
            Assert.True(baseline.TotalMilliseconds <= maxMs, $"baseline test took {baseline.TotalMilliseconds}ms");

            var tasks = new List<Task<TimeSpan>>();

            for (int i = 0; i < 100; i++)
            {
                tasks.Add(Task.Factory.StartNew(func, TaskCreationOptions.LongRunning));
            }

            Task.WaitAll(tasks.ToArray());

            var nbrOverMaxMs = tasks.Count(x => x.Result.TotalMilliseconds > maxMs);
            var maxMsTaken = tasks.Max(x => x.Result.TotalMilliseconds);
            var totalTime = tasks.Sum(x => x.Result.TotalMilliseconds);
            if (nbrOverMaxMs > 0)
            {
                Assert.False(true,
                    $"{nbrOverMaxMs} slower than target speed, slowest took {maxMsTaken}ms and the baseline was {baseline.TotalMilliseconds}ms");
            }
        }


        [Fact]
        public void BasicRequestAndNoResponseTest()
        {
            string ipcId = Guid.NewGuid().ToString("N");
            var expectedMessages = new List<string>()
            {
                "Shutdown from Master",
                "Shutdown from Slave",
            };
            List<string> messages = new List<string>();

            IpcInstance ipcMaster = null;
            IpcInstance ipcSlave = null;
            var masterProcessors = new RequestProcessors();
            var slaveProcessors = new RequestProcessors();
            masterProcessors.RegisterRequestWithNoResponseProcessor<string>(0, (source, request) =>
            {
                messages.Add(request);
            });

            slaveProcessors.RegisterRequestWithNoResponseProcessor<string>(0, (source, request) =>
            {
                messages.Add(request);
            });

            ipcMaster = new IpcInstance(ipcId, masterProcessors);
            Task.Run(() => { ipcSlave = new IpcInstance(ipcId, slaveProcessors); }).Wait();

            var response = ipcMaster.SendRequestWithoutResponse(0, "Shutdown from Master");
            Assert.True(response);
            SpinWait.SpinUntil(() => messages.Count == 1, TimeSpan.FromSeconds(1));
            response = ipcSlave.SendRequestWithoutResponse(0, "Shutdown from Slave");
            Assert.True(response);
            SpinWait.SpinUntil(()=>messages.Count == 2, TimeSpan.FromSeconds(1));
            Assert.Equal(expectedMessages, messages);
            ipcMaster?.Dispose();
            ipcSlave?.Dispose();
        }

        [Fact]
        public void BasicRequestAndErrorResponseTest()
        {
            string ipcId = Guid.NewGuid().ToString("N");
            var expectedMessages = new List<string>()
            {
                "Ping from Slave 0",
                "ErrorType:ApplicationError, ErrorCode:, Message:Exception:Test error from Master",
                "Ping from Slave 1",
                "ErrorType:UnhandledException, ErrorCode:, Message:Exception:The method or operation is not implemented.",
                "ErrorType:ProcessManagerError, ErrorCode:2, Message:No processor registered for command '2' in the request so request rejected.",
                "ErrorType:ProcessManagerError, ErrorCode:3, Message:Send Failed - Usage Report "
            };
            List<string> messages = new List<string>();

            IpcInstance ipcMaster = null;
            IpcInstance ipcSlave = null;
            var masterProcessors = new RequestProcessors();
            var slaveProcessors = new RequestProcessors();
            masterProcessors.RegisterRequestProcessor<string, string>(0, (source, request) =>
            {
                messages.Add(request);
                return new Response<string>() { Error = Error.GenerateError(ErrorType.ApplicationError, new Exception("Test error from Master")) };
            });
            masterProcessors.RegisterRequestProcessor<string, string>(1, (source, request) =>
            {
                messages.Add(request);
                throw new NotImplementedException();
            });


            ipcMaster = new IpcInstance(ipcId, masterProcessors);
            Task.Run(() => { ipcSlave = new IpcInstance(ipcId, slaveProcessors); }).Wait();

            var response = ipcSlave.SendRequest<string, string>(0, "Ping from Slave 0", 1000);
            Assert.True(response.IsError);
            Assert.Null(response.Value);
            Assert.NotNull(response.Error);
            messages.Add(response.Error.ToString());

            response = ipcSlave.SendRequest<string, string>(1, "Ping from Slave 1", 1000);
            Assert.True(response.IsError);
            Assert.Null(response.Value);
            Assert.NotNull(response.Error);
            messages.Add(response.Error.ToString());

            response = ipcSlave.SendRequest<string, string>(2, "Ping from Slave 2", 1000);
            Assert.True(response.IsError);
            Assert.Null(response.Value);
            Assert.NotNull(response.Error);
            messages.Add(response.Error.ToString());

            ipcMaster?.Dispose();
            response = ipcSlave.SendRequest<string, string>(2, "Ping from Slave 3", 1000);
            Assert.True(response.IsError);
            Assert.Null(response.Value);
            Assert.NotNull(response.Error);
            messages.Add(response.Error.ToString().Substring(0, 79));


            Assert.Equal(expectedMessages, messages);

            
            ipcSlave?.Dispose();
        }
    }
}
